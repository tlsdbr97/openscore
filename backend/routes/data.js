const express = require('express');
const router = express.Router();
const User = require('../models/user');
var parseDKU = require('../addons/parseDKU')

// parsing data
router.post('/parseList', (req, res, next) => {
  console.log('ok');
  var username = req.body.user.username;
  console.log(username);
  var password = req.body.user.password;
  console.log(password);
  return new Promise((resolve, reject) => {
    parseDKU.authenticateAndParse(username, password, 'https://webinfo.dankook.ac.kr/tiac/univ/lssn/ttmg/views/findTkcrsTmtblList.do?_view=ok&&sso=ok')
      .then($ => {
        if ($('#tmtblDscAplListTbl')) {
          var docs = [];
          const selector = '#tmtblDscAplListTbl td';
          const td = $(selector); //table에 td가 총 10개있다.
          const tdLength = td.length / 10;
          for (let i = 0; i < tdLength; i++) {
            var scoreInfo = {
              stu_id: username,
              course_code: ($(selector).eq(i * 10 + 1).text().trim()).concat('-' + $(selector).eq(i * 10 + 2).text().trim()),
              course_name: $(selector).eq(i * 10 + 3).text().trim(),
              prof_name: $(selector).eq(i * 10 + 6).text().trim()
            }
            docs.push(scoreInfo);

          }
          console.log(docs)

        }
        //   res.json({docs : docs})
        if (docs.length)
          res.json({
            result: 1,
            docs: docs
          })
        else {
          alert('로그인에 실패하였습니다.')
        }
      })
      .catch(next);
  })

})


// URL => midexam (final goal)

// parseDKU.authenticateAndParse(username, password, 'https://webinfo.dankook.ac.kr/tiac/univ/cret/gdmg/views/findMidExamStuList.do?_view=ok&&sso=ok')
//       .then($ => {
//         if ($('#ltrClGrInpListTbl')) {
//           var docs = [];
//           const selector = '#ltrClGrInpListTbl td';
//           const td = $(selector); //table에 td가 총 7개있다.
//           const tdLength = td.length / 7;
//           for (let i = 0; i < tdLength; i++) {
//             var scoreInfo = {
//               stu_id: username,
//               course_code: ($(selector).eq(i * 7 + 3).text().trim()).concat('-' + $(selector).eq(i * 7 + 4).text().trim()),
//               course_name: $(selector).eq(i * 7 + 5).text().trim(),
//               score: $(selector).eq(i * 7 + 6).text().trim()
//             }
//             docs.push(scoreInfo);



// Data saving function without duplication
function save(stu_id,course_code,course_name,prof_name,callback) {
    User.findOneAndUpdate(
        {
            stu_id: stu_id,
            course_code: course_code,
            course_name: course_name,
            prof_name: prof_name
        },
        {
            stu_id: stu_id,
            course_code: course_code,
            course_name: course_name,
            prof_name: prof_name
        },
        { upsert: true, new: true, multi: true}, 
        callback
    );
}
// Create new student's data
router.post('/getlist', (req, res, next) => {
    return new Promise((resolve,rejection) => {
        for(let i =0; i<req.body.length; i++){
                save(req.body[i].stu_id, req.body[i].course_code, req.body[i].course_name, req.body[i].prof_name, function(err, data){
                    if(err) {
                        console.log(err)
                    } else {
                        console.log(data);
                    }
                })
        }
        res.json({result : 1})
    })
   
})
// Find All by stu_id
router.post('/showdata', (req, res, next) => {
  User.findAllByStuid(req.body.params)
    .then((user) => {
      if (!user) return res.status(404).send({
        err: 'Stu not found'
      })
      res.json({
        users: user
      })
    })
    .catch(err => res.status(500).send(err));
});

// Find All by course_code
// If someone click courseName, We need to distinguish courseCode 
router.post('/course_code', (req, res, next) => {
  User.findAllByCourseCode(req.body.params)
    .then((user) => {
      if (!user) return res.status(404).send({
        err: 'Coursecode not found'
      })
      res.json({
        users: user
      })
    })
})


module.exports = router;