var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userInfo = new Schema({
    stu_id : 'String',
    course_code : 'String',
    course_name : 'String',
    // course_score : {type:'String',default:'90'},
    prof_name : 'String'
})


// Find All by stu_id 
userInfo.statics.findAllByStuid = function(stu_id) {
    return this.find({stu_id})
};

// Find All by course_code
userInfo.statics.findAllByCourseCode = function(course_code) {
    return this.find({course_code})
}


module.exports = mongoose.model('user', userInfo);