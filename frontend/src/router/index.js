import Vue from 'vue'
import Router from 'vue-router'
import DKUlogin from '@/components/DKUlogin'
import openRelease from '@/components/CourseList'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'DKUlogin',
      component: DKUlogin,
      props: true
    },
    {
      path: '/login/openScore',
      name: '/login/openScore',
      component : openRelease,
      props: true
    }
  ]
})
